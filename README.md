# Reproducability-project 
CS4240 Deep Learning (2022/23 Q3) Reproducability project code:

Needed Libraries: 
- OS (3.11.2)
- Numpy (1.11.2)
- PIL (.)
- random 

To run the code, 
- open the 'picture_gen.py' file.
- change the 'path' variable in the 'create_repository' function.
- change parameters in 'if __name__ == __main__' loop. 
- run the code and find your newly created directories on the specified location.
